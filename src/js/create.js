async function createGuest() {

    let firstName = document.getElementById("firstName").value;
    let lastName = document.getElementById("lastName").value;
    let emailAddress = document.getElementById("email").value;
    let address = document.getElementById("address").value;
    let country = document.getElementById("country").value;
    let state = document.getElementById("state").value;
    let phoneNumber = document.getElementById("tel").value;

    let guest = {
        firstName: firstName,
        lastName: lastName,
        email: emailAddress,
        address: address,
        country: country,
        state: state,
        tel: phoneNumber
    }

    let response = await fetch("http://localhost:8090/api/customer", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(guest)
    });

    let data = await response.json();

    window.location.href = "/logado.html";
}