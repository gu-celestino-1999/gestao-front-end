
function resultado(dado) {   
    const result = document.querySelector('.result');
    result.innerHTML = "Price: R$ " + dado;
}

// PEGA N° DE CAMAS
const beds = document.querySelector('.beds');
beds.addEventListener('change', (event) =>{

    var preco_cama = `${event.target.value}`;
    calcula(preco_cama);
});

function difDias(){
    var dataUm = new Date(document.querySelector('.datai').value);
    var dataDois = new Date(document.querySelector('.dataf').value);
    return parseInt((dataDois - dataUm) / (24 * 3600 * 1000));
}

function dias(){
    if (isNaN(difDias())) {
        result.innerHTML = "Select check-out date"
    }else{
        calcula(difDias());
    }
}

function calcula(x) {
    var valor = parseInt(x * 100);
    resultado(valor);
}