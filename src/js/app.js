
async function getGuest () {
    return await (await fetch('http://localhost:8090/api/customer')).json();
}

let guests = [];
let guest_page = [];
let limit = 10;
let page = 0;

var data;

let input = document.querySelector('input');
const tBody = document.querySelector(".load-guest");
const next = document.querySelector(".next");
const previous = document.querySelector(".previous");
const show = document.querySelector(".show-numbers");

(async () => {
    data = await getGuest()
    //Criando um array com os elementos da tabela & da API
    for (let index = 0; index < data.length; index++) {
        const element = data[index];
        let tr = document.createElement("tr")
        tr.innerHTML = `<td>${element.id}</td><td>${element.firstName}</td><td>${element.lastName}</td><td>${element.email}</td><td>${element.address}</td><td>${element.country}</td><td>${element.state}</td><td>${element.tel}</td>`
        guests.push(tr)
    }
    //Quebrando o array em pedaços com 10 elementos cada, o ultimo sempre fica com o que sobra
    for (let index = 0; index < data.length; index += limit) {
        guest_page.push(guests.slice(index, index + limit));
    }
    //Colocando os primeiros dados
    for (let index = 0; index < guest_page[page].length; index++) {
        tBody.appendChild(guest_page[page][index])
        show.innerHTML = "Showing 10 results on page 1";
    }

    //Função do botão next
    next.addEventListener("click", () => {
        page += 1;
        if (guest_page[page]) {
            tBody.innerHTML = "";
            for (let index = 0; index < guest_page[page].length; index++) {
                tBody.appendChild(guest_page[page][index])
            }
            show.innerHTML = "Showing "+guest_page[page].length+" results on page "+(page+1);
        }
    })
    //Função do botão last
    previous.addEventListener("click", () => {
        page -= 1;
        if (guest_page[page]) {
            tBody.innerHTML = "";
            for (let index = 0; index < guest_page[page].length; index++) {
                tBody.appendChild(guest_page[page][index])
            }
            show.innerHTML = "Showing "+guest_page[page].length+" results on page "+(page+1);
        }
    })

    //Função do Search

    input.oninput = handleInput;

    function handleInput(e) {
        let text = e.target.value.toLowerCase();  
        if (text.length == 0) {
            tBody.innerHTML = "";
            for (let index = 0; index < guest_page[page].length; index++) {
                tBody.appendChild(guest_page[page][index])
                show.innerHTML = "Showing "+guest_page[page].length+" results on page "+(page+1);
            }
        }else {
            let result = [];
            let filter = data.map(guests => guests.firstName.toLowerCase() == text || guests.lastName.toLowerCase() == text || guests.emailAddress.toLowerCase() == text)
            //Pegando os resultados
            for (let index = 0; index < filter.length; index++) {
                if (filter[index]) {
                    result.push(data[index])
                }
            }
            //Montando os resultados

            let guest_result = [];
            for (let index = 0; index < result.length; index++) {
                const element = result[index];
                let tr = document.createElement("tr")
                tr.innerHTML = `<td>${element.id}</td><td>${element.firstName}</td><td>${element.lastName}</td><td>${element.emailAddress}</td><td>${element.address}</td><td>${element.country}</td><td>${element.state}</td><td>${element.phoneNumber}</td>`
                guest_result.push(tr)
                tBody.innerHTML = "";
                
            }
            //Mostrando os resultados
            if (guest_result.length > 0) {
                for (let index = 0; index < guest_result.length; index++) {
                    tBody.appendChild(guest_result[index])
                }    
            }else{
                tBody.innerHTML = "";
                let tr = document.createElement("tr")
                tr.innerHTML = `<td colspan='8'>no guests found</td>`
                tBody.appendChild(tr)
                show.innerHTML = "Showing 0 results";
            }
        }
    }   
    
})()
